
const {MongoClient, ObjectID} = require('mongodb');


MongoClient.connect('mongodb://localhost:27017/TodoApp', (error, db) => {
  if(error) {
    return console.log('unable to connect to mongodb server');
  }
  console.log('connected to mongodb server');

  // db.collection('Todos').findOneAndUpdate({
  //   _id: new ObjectID('58f6a9a7a8e0186dee74fc81'
  // )}, {
  //   $set: {
  //     completed: true
  //   }
  // }, {
  //   $returnOriginal: false
  // }).then((res) => {
  //   console.log(res);
  // });

  db.collection('Users').findOneAndUpdate({
    _id: new ObjectID('58f6b20d3c26861fbd4fa8c7'
  )}, {
    $set: {
      name: 'Martin'
    },
    $inc: {
      age: 1
    }
  }, {
    $returnNewDocument: true
  }).then((res) => {
    console.log(res);
  });


   //db.close();
});
