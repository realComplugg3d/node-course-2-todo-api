//const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

// var obj = new ObjectID();

// var user = {name: 'Erick', age: 26};
//
// var {name} = user;

// console.log(obj);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (error, db) => {
  if(error) {
    return console.log('unable to connect to mongodb server');
  }
  console.log('connected to mongodb server');

  // db.collection('Todos').insertOne({
  //
  // }, (err, res) => {
  //   if(err) {
  //     return console.log('unable to insert todo', err);
  //   }
  //
  //   console.log(JSON.stringify(res.ops, undefined, 2));
  // });

  db.collection('Users').insertOne({
    name: 'Erick',
    age: 26,
    location: 'Zapopan'
  }, (err, res) => {
    if(err) {
      return console.log('unable to insert user', err);
    }

    console.log(JSON.stringify(res.ops[0]._id.getTimestamp(), undefined, 2));


  });

  db.close();
});
