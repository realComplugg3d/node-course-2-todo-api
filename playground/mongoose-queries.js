const {ObjectId} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

 var id = '58f7f5741f949d101749c2c5';
//
// if(!ObjectId.isValid(id)) {
//   console.log('Id not valid');
// }

// Todo.find({
//   _id: id
// }).then((todos) => {
//   console.log('todos', todos);
// });
//
// Todo.findOne({
//   _id: id
// }).then((todo) => {
//   console.log('todo', todo);
// });

// Todo.findById(id).then((todo) => {
//   if(!todo) {
//     return console.log('Todo not found');
//   }
//   console.log('todo by id', todo);
// }).catch((e) => {
//   console.log(e);
// });

User.findById(id).then((user) => {
  if(!user) {
    return console.log('User not found');
  }

  console.log('User', user);
}).catch((e) => {
  console.log(e);
});
