
const {MongoClient, ObjectID} = require('mongodb');


MongoClient.connect('mongodb://localhost:27017/TodoApp', (error, db) => {
  if(error) {
    return console.log('unable to connect to mongodb server');
  }
  console.log('connected to mongodb server');
  //deleteMany
    // db.collection('Todos').deleteMany({text: 'Eat lunch'}).then((res) => {
    //   console.log(res);
    // });
    //deleteOne
    // db.collection('Todos').deleteOne({text: 'Eat lunch'}).then((res) => {
    //   console.log(res);
    // });
    //findOneAndDelete
    // db.collection('Todos').findOneAndDelete({completed: false}).then((res) => {
    //   console.log(res);
    // });

    db.collection('Users').deleteMany({name: 'Erick'}).then((res) => {
      console.log(res);
    });
    db.collection('Users').findOneAndDelete({_id: new ObjectID('58ec243eccb48b19012c0a56')}).then((res) => {
      console.log(res);
    });

   //db.close();
});
